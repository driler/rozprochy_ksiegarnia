package pl.edu.agh.ki.sr.bookstore;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.ki.sr.bookstore.actors.BookTextActor;
import pl.edu.agh.ki.sr.bookstore.actors.OrderActor;
import pl.edu.agh.ki.sr.bookstore.actors.SearchActor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BookStore {


    public static void main(String[] args) throws IOException {
        Logger logger = LogManager.getLogger("Bookstore");

        File configFile = new File("bookstore_app.conf");
        Config config = ConfigFactory.parseFile(configFile);

        final ActorSystem bookstore = ActorSystem.create("bookstore", config);
        ActorRef searchActor = bookstore.actorOf(Props.create(SearchActor.class), "search");
        ActorRef orderActor = bookstore.actorOf(Props.create(OrderActor.class), "order");
        ActorRef textActor = bookstore.actorOf(Props.create(BookTextActor.class), "text");


        // interaction
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line = br.readLine();
            logger.log(Level.INFO, line);
            if(line.startsWith("order"))
                orderActor.tell(line, null);
            else if(line.startsWith("search"))
                searchActor.tell(line, null);
            else if(line.startsWith("text"))
                textActor.tell(line, null);
            if (line.equals("q")) {
                break;
            }

        }
    }
}
